//Create a function that accepts a string as an argument. 
// The function must move all capital letters to the front of a word, lowercase letters thereafter and lastly, numbers to the back. It should return a string with the reordered word.
// Examples

// reorder("hA2p4Py") ➞ "APhpy24"

// reorder ("m11oveMENT") ➞ "MENTmove11"

// reorder ("s9hOrt4CAKE") ➞ "OCAKEshrt94"

// Notes
// Keep the original relative order of the upper and lower case letters the same as well as numbers

function reorder(string){
  const matchLowercase = new RegExp('[a-z]', 'g')
  const matchUppercase = new RegExp('[A-Z]', 'g')
  const matchNumbers = new RegExp('[0-9]', 'g')

  const number = string.match(matchNumbers)
  const lowercase = string.match(matchLowercase)
  const uppercase = string.match(matchUppercase)

  let reordered =""
  uppercase.forEach(element => {
    reordered +=element
  }); 
  lowercase.forEach(element => {
    reordered +=element
  });
  number.forEach(element => {
    reordered +=element
  });
  return reordered
}

console.log(reorder("hA2p4Py"))
console.log(reorder("m11oveMENT"))
console.log(reorder("s9hOrt4CAKE"))
