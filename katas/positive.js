// Write a function that returns true if both numbers are:
// •    Smaller than 0, OR ...
// •    Greater than 0, OR ...
// •    Exactly 0
// Otherwise, return false.
// Examples
// both(6, 2) ➞ true

// both(0, 0) ➞ true

// both(-1, 2) ➞ false

// both(0, 2) ➞ false
// Notes
// Inputs will always be two numbers.

function checkPositivity(one, two) {
  if(one >0 && two >0) {
    return true
  }
  if( one ==0 &&  two==0) {
    return true
  }
  if( one <0 &&  two<0) {
    return true
  }
  return false
}

console.log(checkPositivity(6,2))
console.log(checkPositivity(0,0))
console.log(checkPositivity(-1,2))
console.log(checkPositivity(0,2))
console.log("version 2");
// version 2
// Inputs will NOT always be two numbers.
// both("0", 2) --> false

function checkPositivity_versionTwo(one, two) {
  if(isNaN(one) || isNaN(two)) return false
  if(one >0 && two >0) {
    return true
  }
  if( one ==0 &&  two==0) {
    return true
  }
  if( one <0 &&  two<0) {
    return true
  }
  return false
}

console.log(checkPositivity_versionTwo(6,2))
console.log(checkPositivity_versionTwo(0,0))
console.log(checkPositivity_versionTwo(-1,2))
console.log(checkPositivity_versionTwo(0,2))
console.log(checkPositivity_versionTwo("0",2))



