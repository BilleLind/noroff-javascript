



bestFriend = (string, letter, lettersBestfriend) => {
  stringArray = string.split("")
  if(!stringArray.includes(letter) || !stringArray.includes(lettersBestfriend)) return false

  for(i = 0; i <stringArray.length; i++ ) {
    if(stringArray[i] == letter) {
      if(stringArray[i+1] == lettersBestfriend) continue
      else return false
    }
  }
  return true
}


console.log(bestFriend("he headed to the store", "h", "e"))

console.log(bestFriend("i found an ounce with my hound", "o", "u"))

console.log(bestFriend("we found your dynamite", "d", "y"))
