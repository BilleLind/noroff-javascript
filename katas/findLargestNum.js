/*
Create a function that takes an array of numbers and returns the second largest number.
Examples
secondLargest([10, 40, 30, 20, 50]) ➞ 40
 
secondLargest([25, 143, 89, 13, 105]) ➞ 105
 
secondLargest([54, 23, 11, 17, 10]) ➞ 23
 
secondLargest([1, 1]) ➞ 0
 
secondLargest([1]) ➞ 1
 
secondLargest([]) ➞ 0
 
Notes
If only one number exists, return that number

When array only has two numbers that are equal, return 0

Return 0 for an empty array
*/

secondLargest = (array) => {
	let largest = 0,
		secondLargest = 0

	array.forEach((element) => {
		//initial set of variables, also ensure that they exist
		if (largest == 0) {
			largest = element
		} else if (secondLargest == 0) {
			secondLargest = element
		} else {

		//check if element is larger than largest, if it is then move largest to secondlargest
		// => if it isn't greater than the largest, then check if greater than secondLargest.
		if (element > largest) {
			secondLargest = largest
			largest = element
		}
		else if (element > secondLargest  && element == largest) {
			secondLargest = element
		}
  }
	})
	return secondLargest
}

console.log(secondLargest([10, 40, 30, 20, 50]))

console.log(secondLargest([25, 143, 89, 13, 105]))
console.log(secondLargest([54, 23, 11, 17, 10]))
console.log(secondLargest([1,1]))
console.log(secondLargest([1]))
console.log(secondLargest([]))

//Tobias Møller cooler solution
function secondLargest(input){
  if(input.length == 0) return 0;
  if(input.length == 1) return input[0];
  if(input.length == 2 && input[0]==input[1]) return 0;
  input.sort((a, b) => b - a);
  return input[1];
}
