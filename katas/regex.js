// Interview, language_fundamentals, loops, strings

// Create a function that takes two strings as arguments and returns the number of times the first string (the single character) is found in the second string.
// Examples
// charCount("a", "edabit") ➞ 1

// charCount("c", "Chamber of secrets") ➞ 1

// charCount("B", "boxes are fun") ➞ 0

// charCount("b", "big fat bubble") ➞ 4

// charCount("e", "javascript is good") ➞ 0

// charCount("!", "!easy!") ➞ 2
// Notes
// Your output must be case-sensitive (see second example).
// You can get the length of a string with the .length property


function charCount(char, string) {
  const regex = new RegExp(char, 'g')

  const count = string.match(regex)

  if(count != null) return count.length

  return 0
}


console.log(charCount("a", "edabit"));
console.log(charCount("c", "Chamber of secrets"));
console.log(charCount("B", "boxes are fun"));
console.log(charCount("b", "big fat bubble"));
console.log(charCount("e", "javascript is good"));
console.log(charCount("!", "!easy!"));
